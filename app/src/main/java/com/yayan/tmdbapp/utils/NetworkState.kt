package com.yayan.tmdbapp.utils


enum class ResponseStatus {
    SUCCESS,
    ERROR,
    LOADING
}

data class NetworkState<out T>(
    val status: ResponseStatus,
    val data: T?,
    val message: String?
) {
    companion object {
        fun <T> Success(data: T, message: String? = null): NetworkState<T> =
            NetworkState(status = ResponseStatus.SUCCESS, data = data, message = message)

        fun <T> Error(data: T?, message: String): NetworkState<T> =
            NetworkState(status = ResponseStatus.ERROR, data = data, message = message)

        fun <T> Loading(data: T?): NetworkState<T> =
            NetworkState(status = ResponseStatus.LOADING, data = null, message = null)
    }
}
