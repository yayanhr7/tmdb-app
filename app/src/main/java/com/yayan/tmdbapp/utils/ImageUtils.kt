package com.yayan.tmdbapp.utils

import android.widget.ImageView
import com.bumptech.glide.Glide
import com.yayan.tmdbapp.utils.Constants.IMAGE_URL

object ImageUtils {
    fun bindToImageView(imageView: ImageView, url: String) {
        Glide.with(imageView.context)
            .asBitmap()
            .load(IMAGE_URL + url)
            .into(imageView)
    }
}