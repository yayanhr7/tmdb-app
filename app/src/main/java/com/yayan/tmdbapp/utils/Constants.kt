package com.yayan.tmdbapp.utils

object Constants {
    const val IMAGE_URL = "https://image.tmdb.org/t/p/w342"

    const val GENRES_URL = "genre/movie/list"
    const val MOVIES_URL = "discover/movie"
    const val MOVIE_DETAIL = "movie/{movie_id}"
    const val MOVIE_REVIEWS = "movie/{movie_id}/reviews"
    const val MOVIE_VIDEOS = "movie/{movie_id}/videos"

    const val GENRE_ID = "genre_id"
    const val GENRE_NAME = "genre_name"
}