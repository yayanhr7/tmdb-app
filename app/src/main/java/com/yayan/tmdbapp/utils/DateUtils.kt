package com.yayan.tmdbapp.utils

import java.text.SimpleDateFormat
import java.util.Locale

object DateUtils {
    fun convertDate(date: String): String{
        return try {
            val fromFormat = SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH)
            val toFormat = SimpleDateFormat("dd MMMM yyyy")
            toFormat.format(fromFormat.parse(date))

        }catch (e: Exception){
            ""
        }
    }
}