package com.yayan.tmdbapp.di

import com.yayan.tmdbapp.data.remote.api.TMDBApiServices
import com.yayan.tmdbapp.data.remote.repository.genre.GenreRepository
import com.yayan.tmdbapp.data.remote.repository.movie.MoviesRepository
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object RepositoryModule {

    @Singleton
    @Provides
    fun provideGenresRepository(
        tmdbApiServices: TMDBApiServices
    ): GenreRepository = GenreRepository(
        tmdbApiServices
    )

    @Singleton
    @Provides
    fun provideMoviesRepository(
        tmdbApiServices: TMDBApiServices
    ): MoviesRepository = MoviesRepository(
        tmdbApiServices
    )

}