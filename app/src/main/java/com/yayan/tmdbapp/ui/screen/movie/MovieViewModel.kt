package com.yayan.tmdbapp.ui.screen.movie

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import androidx.paging.PagingData
import com.yayan.tmdbapp.data.domain.usecase.movie.MovieUseCase
import com.yayan.tmdbapp.data.model.movie.MovieDetail
import com.yayan.tmdbapp.data.model.movie.MovieItem
import com.yayan.tmdbapp.data.model.movie.ReviewItem
import com.yayan.tmdbapp.data.model.movie.Videos
import com.yayan.tmdbapp.utils.NetworkState
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class MovieViewModel @Inject constructor(private val movieUseCase: MovieUseCase) : ViewModel() {

    private val _movies = MutableLiveData<NetworkState<PagingData<MovieItem>>>()
    val movieResponse: LiveData<NetworkState<PagingData<MovieItem>>>
        get() = _movies

    private val _movieDetail = MutableLiveData<NetworkState<MovieDetail>>()
    val movieDetailResponse: LiveData<NetworkState<MovieDetail>>
        get() = _movieDetail

    private val _trailers = MutableLiveData<NetworkState<Videos>>()
    val trailersResponse: LiveData<NetworkState<Videos>>
        get() = _trailers

    private val _reviews = MutableLiveData<NetworkState<PagingData<ReviewItem>>>()
    val reviewResponse: LiveData<NetworkState<PagingData<ReviewItem>>>
        get() = _reviews

    fun fetchMovies(genreId: Int) {
        try {
            _movies.value = NetworkState.Loading(null)
            viewModelScope.launch {
                movieUseCase.getMovies(genreId).collect {
                    _movies.value = NetworkState.Success(it)
                }

            }
        } catch (e: Exception) {
            _movies.value = NetworkState.Error(null, "")
        }
    }

    fun fetchReviews(movieId: Int) {
        try {
            _reviews.value = NetworkState.Loading(null)
            viewModelScope.launch {
                movieUseCase.getReviews(movieId).collect {
                    _reviews.value = NetworkState.Success(it)
                }

            }
        } catch (e: Exception) {
            _reviews.value = NetworkState.Error(null, "")
        }
    }

    fun fetchMovieDetail(movieId: Int) {
        try {
            _movieDetail.value = NetworkState.Loading(null)
            viewModelScope.launch {
                movieUseCase.getMovieDetail(movieId).collect {
                    _movieDetail.value = NetworkState.Success(it)
                }

            }
        } catch (e: Exception) {
            _movieDetail.value = NetworkState.Error(null, "")
        }

    }

    fun fetchTrailers(movieId: Int) {
        try {
            _movieDetail.value = NetworkState.Loading(null)
            viewModelScope.launch {
                movieUseCase.getTrailers(movieId).collect {
                    _trailers.value = NetworkState.Success(it)
                }
            }
        } catch (e: Exception) {
            _trailers.value = NetworkState.Error(null, "")
        }

    }
}