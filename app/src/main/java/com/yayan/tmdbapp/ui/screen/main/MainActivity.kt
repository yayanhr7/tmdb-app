package com.yayan.tmdbapp.ui.screen.main

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import androidx.activity.viewModels
import androidx.recyclerview.widget.GridLayoutManager
import com.yayan.tmdbapp.databinding.ActivityMainBinding
import com.yayan.tmdbapp.ui.adapter.genre.GenreAdapter
import com.yayan.tmdbapp.utils.ResponseStatus
import dagger.hilt.android.AndroidEntryPoint
import timber.log.Timber

@AndroidEntryPoint
class MainActivity : AppCompatActivity() {

    private val viewModel: GenreViewModel by viewModels()
    private lateinit var binding: ActivityMainBinding

    private lateinit var genreAdapter: GenreAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        viewModel.fetchGenre()
        observeData()
        initView()
    }
    private fun initView() {
        genreAdapter = GenreAdapter()
        val gridLayoutManager = GridLayoutManager(this, 3)

        with(binding){
            rvGenres.apply {
                layoutManager = gridLayoutManager
                adapter = genreAdapter
            }
        }
    }

    private fun observeData() {
        viewModel.genresResponse.observe(this) {
            when(it.status){
                ResponseStatus.SUCCESS -> {
                    binding.progressBar.visibility = View.GONE
                    val data = it.data!!.genreItems
                    genreAdapter.submitList(data)
                }

                ResponseStatus.LOADING-> {
                    binding.progressBar.visibility = View.VISIBLE
                }

                ResponseStatus.ERROR -> {
                    binding.progressBar.visibility = View.GONE
                    Timber.d(it.message)
                }
            }
        }
    }
}