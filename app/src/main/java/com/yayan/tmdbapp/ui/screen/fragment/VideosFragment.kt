package com.yayan.tmdbapp.ui.screen.fragment

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.yayan.tmdbapp.databinding.FragmentVideosBinding
import com.yayan.tmdbapp.ui.screen.movie.MovieViewModel
import dagger.hilt.android.AndroidEntryPoint
import androidx.fragment.app.viewModels
import androidx.recyclerview.widget.LinearLayoutManager
import com.yayan.tmdbapp.ui.adapter.movie.VideoAdapter
import com.yayan.tmdbapp.utils.ResponseStatus
import timber.log.Timber

private const val ARG_PARAM1 = "param1"

@AndroidEntryPoint
class VideosFragment : Fragment() {

    private var movieId: Int = 0
    private val viewModel: MovieViewModel by viewModels()
    private lateinit var videoAdapter: VideoAdapter
    private lateinit var binding: FragmentVideosBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            movieId = it.getInt(ARG_PARAM1)
        }
    }


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentVideosBinding.inflate(layoutInflater, container, false)

        viewModel.fetchTrailers(movieId)
        with(binding){
            videoAdapter = VideoAdapter()
            rvTrailers.apply {
                layoutManager = LinearLayoutManager(requireContext())
                adapter = videoAdapter
            }
        }

        viewModel.trailersResponse.observe(viewLifecycleOwner){
            when(it.status){
                ResponseStatus.SUCCESS -> {
                    val data = it.data!!.results.filter {
                        it.type.equals("trailer", true)
                    }
                    if (data.isEmpty()){
                        binding.tvEmpty.visibility = View.VISIBLE
                    }else {
                        binding.tvEmpty.visibility = View.GONE
                        videoAdapter.submitList(data)
                    }
                }

                ResponseStatus.LOADING -> {
                    Timber.d("loading")
                }

                ResponseStatus.ERROR -> {
                    Timber.d(it.message)
                }
            }
        }
        return binding.root
    }

    companion object {
        @JvmStatic
        fun newInstance(movieId: Int) =
            VideosFragment().apply {
                arguments = Bundle().apply {
                    putInt(ARG_PARAM1, movieId)
                }
            }
    }
}