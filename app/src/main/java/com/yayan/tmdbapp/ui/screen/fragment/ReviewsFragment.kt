package com.yayan.tmdbapp.ui.screen.fragment

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.viewModels
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import com.yayan.tmdbapp.R
import com.yayan.tmdbapp.databinding.FragmentReviewsBinding
import com.yayan.tmdbapp.databinding.FragmentVideosBinding
import com.yayan.tmdbapp.ui.adapter.movie.ReviewAdapter
import com.yayan.tmdbapp.ui.adapter.movie.VideoAdapter
import com.yayan.tmdbapp.ui.screen.movie.MovieViewModel
import com.yayan.tmdbapp.utils.ResponseStatus
import dagger.hilt.android.AndroidEntryPoint
import timber.log.Timber

private const val ARG_PARAM1 = "param1"

@AndroidEntryPoint
class ReviewsFragment : Fragment() {

    private var movieId: Int = 0
    private val viewModel: MovieViewModel by viewModels()
    private lateinit var reviewAdapter: ReviewAdapter
    private lateinit var binding: FragmentReviewsBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            movieId = it.getInt(ARG_PARAM1)
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentReviewsBinding.inflate(layoutInflater, container, false)

        viewModel.fetchReviews(movieId)

        reviewAdapter = ReviewAdapter()
        val gridLayoutManager = LinearLayoutManager(requireContext())

        with(binding) {
            rvReviews.apply {
                layoutManager = gridLayoutManager
                adapter = reviewAdapter
            }
        }

        viewModel.reviewResponse.observe(viewLifecycleOwner) {
            when (it.status) {
                ResponseStatus.SUCCESS -> {
                    val data = it.data!!
                    reviewAdapter.submitData(lifecycle, data)
                    reviewAdapter.addLoadStateListener { combinedLoadStates ->
                        if (combinedLoadStates.append.endOfPaginationReached) {
                            if (reviewAdapter.itemCount < 1) {
                                binding.tvEmpty.visibility = View.VISIBLE
                            } else {
                                binding.tvEmpty.visibility = View.GONE
                            }
                        }
                    }
                }

                ResponseStatus.LOADING -> {
                    Timber.d("loading")
                }

                ResponseStatus.ERROR -> {
                    Timber.d(it.message)
                }
            }
        }

        return binding.root
    }

    companion object {
        @JvmStatic
        fun newInstance(param1: Int) =
            ReviewsFragment().apply {
                arguments = Bundle().apply {
                    putInt(ARG_PARAM1, param1)
                }
            }
    }
}