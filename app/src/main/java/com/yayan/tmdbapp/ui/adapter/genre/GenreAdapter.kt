package com.yayan.tmdbapp.ui.adapter.genre

import android.annotation.SuppressLint
import android.content.Intent
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.yayan.tmdbapp.data.model.genre.GenreItem
import com.yayan.tmdbapp.utils.Constants.GENRE_ID
import com.yayan.tmdbapp.utils.Constants.GENRE_NAME
import com.yayan.tmdbapp.databinding.GenreItemBinding
import com.yayan.tmdbapp.ui.screen.movie.MovieActivity


class GenreAdapter : ListAdapter<GenreItem, GenreAdapter.GenreViewHolder>(CALLBACK) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): GenreViewHolder {
        val binding = GenreItemBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return GenreViewHolder(binding)
    }

    override fun onBindViewHolder(holder: GenreViewHolder, position: Int) {
        val item = getItem(position)
        holder.bind(item)
    }

    class GenreViewHolder(private val binding: GenreItemBinding) : RecyclerView.ViewHolder(
        binding.root
    ) {
        fun bind(item: GenreItem) {
            with(binding) {
                tvTitle.text = item.name
                itemView.setOnClickListener {
                    val intent = Intent(itemView.context, MovieActivity::class.java)
                    intent.putExtra(GENRE_ID, item.id)
                    intent.putExtra(GENRE_NAME, item.name)
                    itemView.context.startActivity(intent)
                }
            }
        }
    }

    companion object {
        val CALLBACK: DiffUtil.ItemCallback<GenreItem> =
            object : DiffUtil.ItemCallback<GenreItem>() {
                override fun areItemsTheSame(oldUser: GenreItem, newUser: GenreItem): Boolean {
                    return oldUser.id == newUser.id
                }

                @SuppressLint("DiffUtilEquals")
                override fun areContentsTheSame(oldUser: GenreItem, newUser: GenreItem): Boolean {
                    return oldUser == newUser
                }
            }
    }
}
