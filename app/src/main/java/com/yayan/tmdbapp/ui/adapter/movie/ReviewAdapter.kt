package com.yayan.tmdbapp.ui.adapter.movie

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.paging.PagingDataAdapter
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.yayan.tmdbapp.data.model.movie.ReviewItem
import com.yayan.tmdbapp.databinding.ReviewItemBinding
import com.yayan.tmdbapp.utils.ImageUtils


class ReviewAdapter : PagingDataAdapter<ReviewItem, ReviewAdapter.ReviewViewHolder>(CALLBACK) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ReviewViewHolder {
        val binding = ReviewItemBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return ReviewViewHolder(binding)
    }

    override fun onBindViewHolder(holder: ReviewViewHolder, position: Int) {
        val item = getItem(position)
        holder.bind(item!!)
    }

    class ReviewViewHolder(private val binding: ReviewItemBinding) : RecyclerView.ViewHolder(
        binding.root
    ) {
        fun bind(item: ReviewItem) {
            with(binding) {
                tvName.text = item.author
                tvReview.text = item.content
                if(item.authorDetails.avatarPath != null) {
                    ImageUtils.bindToImageView(ivAvatar, item.authorDetails.avatarPath!!)
                }
            }
        }
    }

    companion object {
        val CALLBACK: DiffUtil.ItemCallback<ReviewItem> =
            object : DiffUtil.ItemCallback<ReviewItem>() {
                override fun areItemsTheSame(oldUser: ReviewItem, newUser: ReviewItem): Boolean {
                    return oldUser.id == newUser.id
                }

                @SuppressLint("DiffUtilEquals")
                override fun areContentsTheSame(oldUser: ReviewItem, newUser: ReviewItem): Boolean {
                    return oldUser == newUser
                }
            }
    }
}
