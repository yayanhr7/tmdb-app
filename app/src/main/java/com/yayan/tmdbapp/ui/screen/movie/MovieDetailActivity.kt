package com.yayan.tmdbapp.ui.screen.movie

import android.os.Bundle
import android.view.View
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import com.google.android.material.tabs.TabLayoutMediator
import com.yayan.tmdbapp.databinding.ActivityMovieDetailBinding
import com.yayan.tmdbapp.ui.adapter.viewpager.ViewPagerAdapter
import com.yayan.tmdbapp.ui.screen.fragment.ReviewsFragment
import com.yayan.tmdbapp.ui.screen.fragment.VideosFragment
import com.yayan.tmdbapp.utils.DateUtils
import com.yayan.tmdbapp.utils.ImageUtils
import com.yayan.tmdbapp.utils.ResponseStatus
import dagger.hilt.android.AndroidEntryPoint
import timber.log.Timber

@AndroidEntryPoint
class MovieDetailActivity : AppCompatActivity() {

    private val viewModel: MovieViewModel by viewModels()
    private lateinit var binding: ActivityMovieDetailBinding
    private var movieId = 0
    private val tabTitles = listOf("Trailers", "Reviews")

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMovieDetailBinding.inflate(layoutInflater)
        setContentView(binding.root)

        movieId = intent.extras!!.getInt("movieId", 0)
        viewModel.fetchMovieDetail(movieId)

        with(binding){
            val adapter = ViewPagerAdapter(supportFragmentManager, lifecycle)
            adapter.addFragment(VideosFragment.newInstance(movieId), )
            adapter.addFragment(ReviewsFragment.newInstance(movieId), )
            viewPager.adapter = adapter
            TabLayoutMediator(tabLayout, viewPager) { tab, position ->
                tab.text = tabTitles[position]
            }.attach()
        }

        observeData()
    }



    private fun observeData() {
        viewModel.movieDetailResponse.observe(this){
            when(it.status){
                ResponseStatus.SUCCESS -> {
                    binding.progressBar.visibility = View.GONE
                    val data = it.data!!
                    with(binding){
                        tvTitle.text = data.title
                        val genres = data.genres.joinToString(", ") {
                            it!!.name
                        }
                        tvGenres.text = "Genres : $genres"
                        tvEpisode.text = DateUtils.convertDate(data.releaseDate)
                        tvOverview.text = data.overview
                        tvNumOfVote.text = "${data.voteAverage} ( ${data.voteCount} Votes )"
                        ImageUtils.bindToImageView(ivPoster, data.backdropPath)
                    }
                }
                ResponseStatus.LOADING -> {
                    binding.progressBar.visibility = View.VISIBLE
                }
                ResponseStatus.ERROR -> {
                    binding.progressBar.visibility = View.GONE
                    Timber.d(it.message)
                }
            }
        }
    }
}