package com.yayan.tmdbapp.ui.screen.movie

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import androidx.activity.viewModels
import androidx.recyclerview.widget.GridLayoutManager
import com.yayan.tmdbapp.utils.Constants.GENRE_ID
import com.yayan.tmdbapp.utils.Constants.GENRE_NAME
import com.yayan.tmdbapp.databinding.ActivityMovieBinding
import com.yayan.tmdbapp.ui.adapter.movie.MovieAdapter
import com.yayan.tmdbapp.utils.ResponseStatus
import dagger.hilt.android.AndroidEntryPoint
import timber.log.Timber

@AndroidEntryPoint
class MovieActivity : AppCompatActivity() {

    private lateinit var binding: ActivityMovieBinding
    private val viewModel: MovieViewModel by viewModels()

    private lateinit var movieAdapter: MovieAdapter
    private lateinit var genreName: String

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMovieBinding.inflate(layoutInflater)
        setContentView(binding.root)

        val genreId = intent.extras!!.getInt(GENRE_ID, 0)
        genreName = intent.extras!!.getString(GENRE_NAME, "")

        viewModel.fetchMovies(genreId)
        observeData()
        initView()
        onClickView()
    }

    private fun initView() {
        movieAdapter = MovieAdapter()
        val gridLayoutManager = GridLayoutManager(this, 2)

        with(binding) {
            appBar.title = genreName
            rvMovies.apply {
                layoutManager = gridLayoutManager
                adapter = movieAdapter
            }
        }
    }

    private fun observeData() {
        viewModel.movieResponse.observe(this@MovieActivity) {
            when (it.status) {
                ResponseStatus.SUCCESS -> {
                    binding.progressBar.visibility = View.GONE
                    val data = it.data!!
                    movieAdapter.submitData(lifecycle, data)
                    movieAdapter.addLoadStateListener { combinedLoadStates ->
                        if (combinedLoadStates.append.endOfPaginationReached) {
                            if (movieAdapter.itemCount < 1) {
                                binding.tvEmpty.visibility = View.VISIBLE
                            } else {
                                binding.tvEmpty.visibility = View.GONE
                            }
                        }
                    }
                }

                ResponseStatus.LOADING -> {
                    binding.progressBar.visibility = View.VISIBLE
                }

                ResponseStatus.ERROR -> {
                    binding.progressBar.visibility = View.GONE
                    Timber.d( it.message)
                }
            }
        }
    }


    private fun onClickView() {
        with(binding){
            appBar.setOnClickListener {
                onBackPressedDispatcher.onBackPressed()
            }
        }
    }
}