package com.yayan.tmdbapp.ui.adapter.movie

import android.annotation.SuppressLint
import android.content.Intent
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.paging.PagingDataAdapter
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.yayan.tmdbapp.data.model.movie.MovieItem
import com.yayan.tmdbapp.databinding.MovieItemBinding
import com.yayan.tmdbapp.ui.screen.movie.MovieDetailActivity
import com.yayan.tmdbapp.utils.ImageUtils


class MovieAdapter : PagingDataAdapter<MovieItem, MovieAdapter.MovieViewHolder>(CALLBACK) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MovieViewHolder {
        val binding = MovieItemBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return MovieViewHolder(binding)
    }

    override fun onBindViewHolder(holder: MovieViewHolder, position: Int) {
        val user = getItem(position)
        holder.bind(user!!)
    }

    class MovieViewHolder(private val binding: MovieItemBinding) : RecyclerView.ViewHolder(
        binding.root
    ) {
        fun bind(item: MovieItem) {
            with(binding) {
                ImageUtils.bindToImageView(ivCover, item.posterPath)
                itemView.setOnClickListener {
                    val intent = Intent(itemView.context, MovieDetailActivity::class.java)
                    intent.putExtra("movieId", item.movieId)
                    itemView.context.startActivity(intent)
                }
            }
        }
    }

    companion object {
        val CALLBACK: DiffUtil.ItemCallback<MovieItem> =
            object : DiffUtil.ItemCallback<MovieItem>() {
                override fun areItemsTheSame(oldUser: MovieItem, newUser: MovieItem): Boolean {
                    return oldUser.movieId == newUser.movieId
                }

                @SuppressLint("DiffUtilEquals")
                override fun areContentsTheSame(oldUser: MovieItem, newUser: MovieItem): Boolean {
                    return oldUser == newUser
                }
            }
    }
}
