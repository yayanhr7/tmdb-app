package com.yayan.tmdbapp.ui.screen.main

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.yayan.tmdbapp.data.model.genre.Genres
import com.yayan.tmdbapp.data.domain.usecase.genre.GenreUseCase
import com.yayan.tmdbapp.utils.NetworkState
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class GenreViewModel @Inject constructor(private val genreUseCase: GenreUseCase) : ViewModel() {

    private val _genresList = MutableLiveData<NetworkState<Genres>>()
    val genresResponse: LiveData<NetworkState<Genres>>
        get() = _genresList

    fun fetchGenre() {
        try {
            _genresList.value = NetworkState.Loading(null)
            viewModelScope.launch {
                genreUseCase.getGenre().collect {
                    if (it.genreItems.isNotEmpty()) {
                        _genresList.value = NetworkState.Success(it)
                    } else {
                        _genresList.value = NetworkState.Error(null, "")
                    }
                }
            }
        } catch (e: Exception) {
            _genresList.value = NetworkState.Error(null, "")
        }

    }
}