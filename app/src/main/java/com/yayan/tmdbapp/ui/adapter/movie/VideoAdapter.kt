package com.yayan.tmdbapp.ui.adapter.movie

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.pierfrancescosoffritti.androidyoutubeplayer.core.player.YouTubePlayer
import com.pierfrancescosoffritti.androidyoutubeplayer.core.player.listeners.AbstractYouTubePlayerListener
import com.pierfrancescosoffritti.androidyoutubeplayer.core.player.options.IFramePlayerOptions
import com.yayan.tmdbapp.data.model.movie.VideoItem
import com.yayan.tmdbapp.databinding.VideoItemBinding


class VideoAdapter : ListAdapter<VideoItem, VideoAdapter.VideoViewHolder>(CALLBACK) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): VideoViewHolder {
        val binding = VideoItemBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return VideoViewHolder(binding)
    }

    override fun onBindViewHolder(holder: VideoViewHolder, position: Int) {
        val user = getItem(position)
        holder.bind(user)
    }

    class VideoViewHolder(private val binding: VideoItemBinding) : RecyclerView.ViewHolder(
        binding.root
    ) {
        fun bind(item: VideoItem) {
            with(binding) {
                tvTitle.text = item.name

                wvPlayer.addYouTubePlayerListener(object : AbstractYouTubePlayerListener() {
                    override fun onReady(youTubePlayer: YouTubePlayer) {
                        youTubePlayer.cueVideo(item.key, 0f)
                    }
                })
            }
        }
    }

    companion object {
        val CALLBACK: DiffUtil.ItemCallback<VideoItem> =
            object : DiffUtil.ItemCallback<VideoItem>() {
                override fun areItemsTheSame(oldUser: VideoItem, newUser: VideoItem): Boolean {
                    return oldUser.id == newUser.id
                }

                @SuppressLint("DiffUtilEquals")
                override fun areContentsTheSame(oldUser: VideoItem, newUser: VideoItem): Boolean {
                    return oldUser == newUser
                }
            }
    }
}
