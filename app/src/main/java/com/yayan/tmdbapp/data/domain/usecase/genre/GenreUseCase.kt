package com.yayan.tmdbapp.data.domain.usecase.genre

import com.yayan.tmdbapp.data.remote.repository.genre.GenreRepository
import javax.inject.Inject

class GenreUseCase @Inject constructor(private val repository: GenreRepository) {
    suspend fun getGenre() = repository.getAllGenres()
}