package com.yayan.tmdbapp.data.model.genre

import com.google.gson.annotations.SerializedName

data class GenreItem(
    @SerializedName("id")
    val id: Int,
    @SerializedName("name")
    val name: String
)
