package com.yayan.tmdbapp.data.remote.api

import com.yayan.tmdbapp.data.model.genre.Genres
import com.yayan.tmdbapp.data.model.movie.MovieDetail
import com.yayan.tmdbapp.data.model.movie.Reviews
import com.yayan.tmdbapp.data.model.movie.Videos
import com.yayan.tmdbapp.utils.Constants.GENRES_URL
import com.yayan.tmdbapp.utils.Constants.MOVIES_URL
import com.yayan.tmdbapp.data.model.movie.Movies
import com.yayan.tmdbapp.utils.Constants.MOVIE_DETAIL
import com.yayan.tmdbapp.utils.Constants.MOVIE_REVIEWS
import com.yayan.tmdbapp.utils.Constants.MOVIE_VIDEOS
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

interface TMDBApiServices {
    @GET(GENRES_URL)
    suspend fun getAllGenres() : Genres

    @GET(MOVIES_URL)
    suspend fun getMoviesByGenre(
        @Query("page") page: Int,
        @Query("with_genres") genreId: Int,
    ): Movies

    @GET(MOVIE_DETAIL)
    suspend fun getMovieDetail(
        @Path("movie_id") movieId: Int
    ): MovieDetail


    @GET(MOVIE_REVIEWS)
    suspend fun getReviews(
        @Path("movie_id") movieId: Int,
        @Query("page") page: Int
    ): Reviews

    @GET(MOVIE_VIDEOS)
    suspend fun getVideo(
        @Path("movie_id") movieId: Int
    ): Videos
}