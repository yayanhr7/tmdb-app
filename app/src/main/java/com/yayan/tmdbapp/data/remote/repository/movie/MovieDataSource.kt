package com.yayan.tmdbapp.data.remote.repository.movie

import androidx.paging.PagingData
import com.yayan.tmdbapp.data.model.movie.MovieDetail
import com.yayan.tmdbapp.data.model.movie.MovieItem
import com.yayan.tmdbapp.data.model.movie.ReviewItem
import com.yayan.tmdbapp.data.model.movie.Videos
import kotlinx.coroutines.flow.Flow

interface MovieDataSource {
    fun getGenres(genreId: Int) : Flow<PagingData<MovieItem>>
    fun getReviews(genreId: Int) : Flow<PagingData<ReviewItem>>
    suspend fun getMovieDetail(movieId: Int) : Flow<MovieDetail>
    suspend fun getTrailers(movieId: Int) : Flow<Videos>
}