package com.yayan.tmdbapp.data.model.genre

import com.google.gson.annotations.SerializedName

data class Genres(
    @SerializedName("genres")
    val genreItems: List<GenreItem>
)