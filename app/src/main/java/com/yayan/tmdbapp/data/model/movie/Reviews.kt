package com.yayan.tmdbapp.data.model.movie

import com.google.gson.annotations.SerializedName

data class Reviews(
    @SerializedName("id")
    var id: Int,

    @SerializedName("page")
    var page: Int,

    @SerializedName("results")
    var results: List<ReviewItem>,

    @SerializedName("total_page")
    var totalPage: Int,

    @SerializedName("total_results")
    var totalResults: Int,
)

data class ReviewItem(
    @SerializedName("author")
    var author: String,
    @SerializedName("author_details")
    var authorDetails: AuthorDetails,
    @SerializedName("content")
    var content: String,
    @SerializedName("created_at")
    var createdAt: String,
    @SerializedName("id")
    var id: String,
    @SerializedName("updated_at")
    var updatedAt: String,
    @SerializedName("url")
    var url: String
)

data class AuthorDetails(
    @SerializedName("name")
    var name: String,
    @SerializedName("username")
    var username: String,
    @SerializedName("avatar_path")
    var avatarPath: String? = null,
    @SerializedName("rating")
    var rating: String
)