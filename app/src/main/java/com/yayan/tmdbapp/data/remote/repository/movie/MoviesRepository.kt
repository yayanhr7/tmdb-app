package com.yayan.tmdbapp.data.remote.repository.movie

import androidx.paging.Pager
import androidx.paging.PagingConfig
import androidx.paging.PagingData
import com.yayan.tmdbapp.data.model.movie.MovieDetail
import com.yayan.tmdbapp.data.model.movie.MovieItem
import com.yayan.tmdbapp.data.model.movie.ReviewItem
import com.yayan.tmdbapp.data.model.movie.Videos
import com.yayan.tmdbapp.data.paging.MoviePagingSource
import com.yayan.tmdbapp.data.paging.ReviewPagingSource
import com.yayan.tmdbapp.data.remote.api.TMDBApiServices
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import javax.inject.Inject

class MoviesRepository @Inject constructor(
    private val tmdbApiServices: TMDBApiServices
) : MovieDataSource {

    override fun getGenres(genreId: Int): Flow<PagingData<MovieItem>> {
        return Pager(
            pagingSourceFactory = { MoviePagingSource(tmdbApiServices, genreId) },
            config = PagingConfig(1)
        ).flow
    }

    override fun getReviews(movieId: Int): Flow<PagingData<ReviewItem>> {
        return Pager(
            pagingSourceFactory = { ReviewPagingSource(tmdbApiServices, movieId) },
            config = PagingConfig(1)
        ).flow
    }

    override suspend fun getMovieDetail(movieId: Int): Flow<MovieDetail> {
        return flow {
            emit(
                tmdbApiServices.getMovieDetail(movieId)
            )
        }
    }

    override suspend fun getTrailers(movieId: Int): Flow<Videos> {
        return flow {
            emit(
                tmdbApiServices.getVideo(movieId)
            )
        }
    }

}