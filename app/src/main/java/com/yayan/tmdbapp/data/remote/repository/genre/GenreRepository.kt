package com.yayan.tmdbapp.data.remote.repository.genre

import com.yayan.tmdbapp.data.model.genre.Genres
import com.yayan.tmdbapp.data.remote.api.TMDBApiServices
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import javax.inject.Inject

class GenreRepository @Inject constructor(
    private val tmdbApiServices: TMDBApiServices
) : GenreDataSource {
    override suspend fun getAllGenres(): Flow<Genres> {
        return flow {
            emit(
                tmdbApiServices.getAllGenres()
            )
        }
    }
}