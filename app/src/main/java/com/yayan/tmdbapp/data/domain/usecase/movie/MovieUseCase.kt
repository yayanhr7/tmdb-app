package com.yayan.tmdbapp.data.domain.usecase.movie

import com.yayan.tmdbapp.data.remote.repository.movie.MoviesRepository
import javax.inject.Inject

class MovieUseCase @Inject constructor(private val repository: MoviesRepository) {
    fun getMovies(genreId: Int) = repository.getGenres(genreId)
    fun getReviews(movieId: Int) = repository.getReviews(movieId)
    suspend fun getMovieDetail(movieId: Int) = repository.getMovieDetail(movieId)
    suspend fun getTrailers(movieId: Int) = repository.getTrailers(movieId)
}