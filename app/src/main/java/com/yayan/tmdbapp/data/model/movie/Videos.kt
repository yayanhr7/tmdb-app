package com.yayan.tmdbapp.data.model.movie

import com.google.gson.annotations.SerializedName

data class Videos (
    @SerializedName("id")
    var id: Int,
    @SerializedName("results")
    var results: List<VideoItem>
)

data class VideoItem(
    @SerializedName("name")
    var name: String,
    @SerializedName("key")
    var key: String,
    @SerializedName("site")
    var site: String,
    @SerializedName("size")
    var size: Int,
    @SerializedName("type")
    var type: String,
    @SerializedName("id")
    var id: String
)