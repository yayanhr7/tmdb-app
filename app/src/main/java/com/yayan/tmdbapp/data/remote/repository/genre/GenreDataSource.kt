package com.yayan.tmdbapp.data.remote.repository.genre

import com.yayan.tmdbapp.data.model.genre.Genres
import kotlinx.coroutines.flow.Flow

interface GenreDataSource {
    suspend fun getAllGenres() : Flow<Genres>
}