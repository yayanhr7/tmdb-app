plugins {
    id("com.android.application")
    id("org.jetbrains.kotlin.android")
    id("dagger.hilt.android.plugin")
    id("kotlin-kapt")
}

android {
    namespace = "com.yayan.tmdbapp"
    compileSdk = 34

    defaultConfig {
        applicationId = "com.yayan.tmdbapp"
        minSdk = 24
        targetSdk = 34
        versionCode = 1
        versionName = "1.0"

        testInstrumentationRunner = "androidx.test.runner.AndroidJUnitRunner"
        buildConfigField("String", "TOKEN_KEY", "\"eyJhbGciOiJIUzI1NiJ9.eyJhdWQiOiI0NjA5OGIzNjUzY2FjNjUyNDRmNzM3ZjI5ODNmYzNhOCIsInN1YiI6IjU5OTU3YmUwYzNhMzY4NTZlMTAxMDlmZiIsInNjb3BlcyI6WyJhcGlfcmVhZCJdLCJ2ZXJzaW9uIjoxfQ.nq8R8c5Di0zN_FyISJMBSbo3zEk5Q-kPOclrOQwXpSQ\"")
        buildConfigField("String", "BASE_URL", "\"https://api.themoviedb.org/3/\"")
    }

    buildTypes {
        release {
            isMinifyEnabled = false
            proguardFiles(
                getDefaultProguardFile("proguard-android-optimize.txt"),
                "proguard-rules.pro"
            )
        }
    }
    compileOptions {
        sourceCompatibility = JavaVersion.VERSION_17
        targetCompatibility = JavaVersion.VERSION_17
    }
    kotlinOptions {
        jvmTarget = "17"
    }

    buildFeatures {
        buildConfig = true
        viewBinding = true
    }
}

dependencies {

    implementation("androidx.core:core-ktx:1.9.0")
    implementation("androidx.appcompat:appcompat:1.6.1")
    implementation("com.google.android.material:material:1.8.0")
    implementation("androidx.constraintlayout:constraintlayout:2.1.4")
    testImplementation("junit:junit:4.13.2")
    androidTestImplementation("androidx.test.ext:junit:1.1.5")
    androidTestImplementation("androidx.test.espresso:espresso-core:3.5.1")

    implementation("androidx.navigation:navigation-fragment-ktx:2.7.3")

    //dagger-hilt
    implementation("com.google.dagger:hilt-android:2.48")
    kapt("com.google.dagger:hilt-compiler:2.48")

    //compose-hilt
    implementation("androidx.hilt:hilt-navigation-compose:1.0.0")


    // retrofit
    implementation("com.squareup.retrofit2:retrofit:2.9.0")
    implementation("com.squareup.retrofit2:converter-gson:2.9.0")
    implementation("com.squareup.okhttp3:logging-interceptor:5.0.0-alpha.2")

    //gson
    implementation("com.google.code.gson:gson:2.10.1")

    // timber log
    implementation("com.jakewharton.timber:timber:5.0.1")

    // pagination
    implementation("androidx.paging:paging-runtime-ktx:3.2.1")
    implementation("androidx.paging:paging-compose:3.2.1")

    //glide image
    implementation("com.github.bumptech.glide:glide:4.16.0")

    implementation("com.pierfrancescosoffritti.androidyoutubeplayer:core:12.1.0")

    //circularimageview
    implementation("com.mikhaellopez:circularimageview:4.3.1")


}